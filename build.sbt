import Dependencies._


lazy val commonSettings = Seq(
  scalaVersion := "2.13.8",
  version := "0.1.0-SNAPSHOT",
  organization     := "com.example",
  organizationName := "example",

  libraryDependencies ++= Seq(
    scalaTest % Test
  )
)

lazy val sandbox = (project in file("sandbox"))
  .settings(
    name := "sandbox",

    commonSettings,
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-jdk-http-client" % "0.7.0"
    )
  )

lazy val scribble = (project in file("scribble"))
  .settings(
    name := "scribble",

    commonSettings,
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
